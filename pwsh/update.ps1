import-module au
. $PSScriptRoot\..\_scripts\all.ps1

$releases    = 'https://github.com/PowerShell/PowerShell/releases'
$domain   = $releases -split '(?<=//.+)/' | select -First 1

function global:au_SearchReplace {
   @{
        "$($Latest.PackageName).nuspec" = @{
            "(\<dependency .+?`"powershell-core`" version=)`"([^`"]+)`"" = "`$1`"$($Latest.Version)`""
            "(\<releaseNotes\>).*?(\</releaseNotes\>)" = "`${1}$($Latest.ReleaseNotes)`$2"
        }
    }
}

#function global:au_BeforeUpdate { Get-RemoteFiles -Purge }
function global:au_AfterUpdate  { Set-DescriptionFromReadme -SkipFirst 2 }

function global:au_GetLatest {
    $download_page = Invoke-WebRequest -UseBasicParsing -Uri $releases

    $re  = "PowerShell-.*-win-x64.msi"
    $url = $download_page.links | ? href -match $re | select -First 1 -expand href
    $url = "https://github.com" + $url
    $url32 = $url -replace 'x64.msi$', 'x86.msi'  

    $version = $url -split '-|-win-x64.msi' | select -Last 1 -Skip 2

    return @{
        URL64        = $url
        URL32        = $url32
        Version      = $version.Replace('v','')
        ReleaseNotes = "$releases/tag/${version}"
    }
}

update -ChecksumFor none
